﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

	private CharacterInfoList<ArchEnemy> enemysList;

	public TextAsset enemyJSON;

	public List<GameObject> enemysBodyList;

	public GameObject startingPoint;

	public GameObject endPoint;

	void Awake(){
		this.enemysList = CharacterInfoList<ArchEnemy>.LoadJson(enemyJSON);
	}


	private GameObject FindEnemyBody(ArchEnemy enemy){
		GameObject enemyBody = enemysBodyList.Find(element => element.name == enemy.name);
		return enemyBody;
	}

	public void instantiateEnemy(ArchEnemy enemy){
		GameObject body = this.FindEnemyBody(enemy);
		GameObject enemyCreated = GameObject.Instantiate(body,this.startingPoint.transform,false);
		EnemyAI enemyMind = enemyCreated.GetComponent<EnemyAI>();
		enemyMind.setEndPoint(endPoint);
		enemyMind.setStats(enemy);
		Debug.Log("Inimigo "+ enemy.name + " criado. ");
	}

	public void instantiateRandomEnemy(){
		int quantity = this.enemysList.list.Count;
		ArchEnemy chosenOne = this.enemysList.list[Random.Range(0,quantity)];
		this.instantiateEnemy(chosenOne);
	}

}
