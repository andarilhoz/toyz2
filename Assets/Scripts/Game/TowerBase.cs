﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBase : MonoBehaviour {

	public Tower towerType;

	public List<GameObject> towersLvlOne;

	public List<GameObject> towersLvlTwo;

	public List<GameObject> towersLvlThree;


	public void Initialize(){
		GameObject myPrefab = towersLvlOne.Find(tower => tower.name == towerType.name);
		GameObject myObject = GameObject.Instantiate(myPrefab,
			this.gameObject.transform.position,
			this.transform.rotation,this.gameObject.transform);
		
		Cell.activeCell.tower = this.gameObject;	
	}

}
