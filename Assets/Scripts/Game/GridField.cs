﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridField : MonoBehaviour {

	[SerializeField]
	private GameObject tile;

	[SerializeField]
	private GameObject startingPoint;

	[SerializeField]
	private int linhas;

	[SerializeField]
	private int colunas;

	[SerializeField]
	private TPSSystem TPS;
	private List<Cell> field = new List<Cell>();

	// Use this for initialization
	void Start () {
		for(int c = 1; c <= colunas; c++)
			for(int l = 1; l <= linhas; l++){
				GameObject cell = GameObject.Instantiate(tile,this.transform.position,this.transform.rotation);	
				Cell cellScript = cell.GetComponent<Cell>();

				cellScript.line = l;
				cellScript.column = c;

				cellScript.TPS = TPS;
				cellScript.startingPoint = startingPoint;

				cellScript.Initialize();

				field.Add(cellScript);
			}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
