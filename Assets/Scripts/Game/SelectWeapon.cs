﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectWeapon : MonoBehaviour {
	
	public Image highLigth;
	public Tower myTower;
	public TPSSystem TPS;

	public static SelectWeapon activeNow;

	public GameObject towerBase;

	private GUIManager managerGUI;

	// Use this for initialization
	void OnMouseOver(){
		activeNow = this;
		this.highLigth.enabled = true;
	}
	void OnMouseExit(){
		if(activeNow == this)
			activeNow = null;
		this.highLigth.enabled = false;
	}

	void Start () {
		myTower = TPS.towerList.list.Find( tower => tower.name == this.name);			
		managerGUI = GameObject.Find("MainGameHUD").GetComponent<GUIManager>();
	}
	
	public void CleanSelect(){
		activeNow = null;
		this.highLigth.enabled = false;
	}

	public void Summon(){
		if(this.name == "Help")
			managerGUI.toggleInfoWeapons();
		else
			SummonTower();		
	}

	public void SummonTower(){
		GameObject tower = GameObject.Instantiate(towerBase,
			this.findTowerPosition(),
			this.findTowerRotation());
			
		TowerBase towerScript = tower.GetComponent<TowerBase>();

		towerScript.towerType = myTower;
		towerScript.Initialize();
	}

	public Vector3 findTowerPosition(){
		return Cell.activeCell.findTowerPositionByLineColumnAndStartPoint();
	}
	public Quaternion findTowerRotation(){
		return new Quaternion(0,0,0,0);
	}
}
