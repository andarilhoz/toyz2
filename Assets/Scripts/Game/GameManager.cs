﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject pauseView;
	public GameObject infoWeaponsView;
	public AudioController audioManager;
	public static Fase fase;
	
	public enum State  {
		Game,
		Tutorial,
		InfoWeapons,
		Pause,
		MobileAPI,
		Store,
	};

	private State state;
	public Text faseName;
	
	void Awake(){
		faseName.text = GameManager.fase.ToString();
	}
	IEnumerator TutorialState() {
		Debug.Log("Tutorial: Enter");
		while(state == State.Tutorial) {
			yield return 0;
		}
		Debug.Log("Tutorial: Exited");
		NextState();
	}

	IEnumerator GameState() {
		Debug.Log("Game: Enter");
		while(state == State.Game) {
			yield return 0;
		}
		Debug.Log("Game: Exited");
		NextState();
	}

	IEnumerator InfoWeaponsState() {
		Debug.Log("InfoWeapons: Enter");
		pauseGame();
		infoWeaponsView.SetActive(true);
		while(state == State.InfoWeapons) {
			yield return 0;
		}
		infoWeaponsView.SetActive(false);
		resumeGame();		
		Debug.Log("InfoWeapons: Exited");
		NextState();
	}

	IEnumerator PauseState() {
		Debug.Log("Pause: Enter");
		pauseGame();
		pauseView.SetActive(true);
		while(state == State.Pause) {
			yield return 0;
		}
		pauseView.SetActive(false);
		resumeGame();		
		Debug.Log("Pause: Exited");
		NextState();
	}

	IEnumerator MobileAPIState() {
		Debug.Log("MobileAPI: Enter");
		while(state == State.MobileAPI) {
			yield return 0;
		}
		Debug.Log("MobileAPI: Exited");
		NextState();
	}

	IEnumerator StoreState() {
		Debug.Log("Store: Enter");
		while(state == State.Store) {
			yield return 0;
		}
		Debug.Log("Store: Exited");
		NextState();
	}
	
	void NextState() {
		string methodName = state.ToString() + "State";
		System.Reflection.MethodInfo info = 
			GetType().GetMethod(methodName,
								System.Reflection.BindingFlags.NonPublic |
								System.Reflection.BindingFlags.Instance);
			StartCoroutine((IEnumerator)info.Invoke(this,null));
	}

	public void changeState(State state){
		audioManager.playButtonClicked();
		this.state = state;
	}

	public State getState(){
		return this.state;
	}

	void pauseGame(){
		Time.timeScale = 0;
	}

	void resumeGame(){
		Time.timeScale = 1;
	}

	void Start () {
		NextState();
	}

}
