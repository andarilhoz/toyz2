using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Cell: MonoBehaviour {

    [SerializeField]
    public int timeUntilCreateTPS = 20;

    [SerializeField]
    public Material normalMaterial;

    [SerializeField]
    public Material selectedMaterial;

    public int line;
    public int column;

    public static Cell activeCell;

    public TPSSystem TPS;

    public GameObject startingPoint;

    public GameObject tower;
    

    private int timePress;
    private bool active = false;
    private bool target = false;

    public void Initialize() {
        Vector3 tilePosition = findTowerPositionByLineColumnAndStartPoint();
        this.setTowerPositionBasedOnStartingPoint(tilePosition,startingPoint.transform);
        this.changeCellNameGivenOwnLineAndColumn();
    }

    private void changeCellNameGivenOwnLineAndColumn(){
        this.name = "line: "+line+ " column: "+column;
    }

    private void setTowerPositionBasedOnStartingPoint(Vector3 position, Transform startingPoint){
        this.gameObject.transform.parent = startingPoint;
        this.gameObject.transform.position = position;
    }

    public Vector3 findTowerPositionByLineColumnAndStartPoint(){
        Vector3 floorTopLeft = startingPoint.transform.position;
        
        float sizeTileX = this.gameObject.GetComponent<MeshRenderer>().bounds.size.x;
        float sizeTileZ = this.gameObject.GetComponent<MeshRenderer>().bounds.size.z;

        float posX = floorTopLeft.x + (column * sizeTileX);
        float posZ = floorTopLeft.z + (line * sizeTileZ);

        return new Vector3(posX,floorTopLeft.y+0.5f,posZ);
    }

    void Update(){
        if(target)
            timePress++;
    }

    private void OnMouseDown(){
        if(EventSystem.current.IsPointerOverGameObject())
            return;
        if(this.tower != null)
            return;
        this.target = true;
        activeCell = this;
        timePress = 0;
    }

    private void OnMouseUp(){
        if(this.active)
            this.ToggleMaterial();
        this.target = false;
        this.active = false;
        timePress = 0;
        if(SelectWeapon.activeNow)
            SelectWeapon.activeNow.Summon();
        TPS.Dismiss();
    }

    private void OnMouseDrag(){
        if(timePress > timeUntilCreateTPS && this.active == false){
            this.active = true;
            TPS.Summon(this.transform.position);
            this.ToggleMaterial();
        }
    }

    private void ToggleMaterial(){
        if(this.GetComponent<MeshRenderer>().sharedMaterial == normalMaterial)
            this.GetComponent<MeshRenderer>().material = selectedMaterial;
        else
            this.GetComponent<MeshRenderer>().material = normalMaterial;
    }
}
