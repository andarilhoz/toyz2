﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour {

	private ArchEnemy stats;
	private NavMeshAgent agent;
	private GameObject endPoint;
	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent>();		
	}
	
	// Update is called once per frame
	void Update () {
		agent.SetDestination(endPoint.transform.position);		
	}

	public void setStats(ArchEnemy stats){
		this.stats = stats;
	}

	public void setEndPoint(GameObject endPoint){
		this.endPoint = endPoint;
	}

	void OnTriggerEnter(Collider other){
		if(other.name == endPoint.name)
			this.reachDestination();
	}

	private void reachDestination(){
		Destroy(this.gameObject);
	}
}
