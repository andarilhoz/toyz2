﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

	public GameManager gameManager;
	public AudioController audioManager;
	
	void Awake () {
		this.gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	public void togglePauseGame(){
		audioManager.playButtonClicked();
		if(gameManager.getState() == GameManager.State.Pause)
			gameManager.changeState(GameManager.State.Game);
		else
			gameManager.changeState(GameManager.State.Pause);
	}

	public void toggleInfoWeapons(){
		audioManager.playButtonClicked();
		if(gameManager.getState() == GameManager.State.InfoWeapons)
			gameManager.changeState(GameManager.State.Game);
		else
			gameManager.changeState(GameManager.State.InfoWeapons);
	}

	public void backToMenu(){
		Time.timeScale = 1;
		audioManager.playButtonClicked();
		LoadingManager.scene = 1;
		SceneManager.LoadScene(0);
	}

}
