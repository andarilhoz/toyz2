﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSSystem : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	public TextAsset towerJSON;

	public CharacterInfoList<Tower> towerList;

	void Awake(){
		this.towerList = CharacterInfoList<Tower>.LoadJson(towerJSON);
	}

	public void Dismiss(){
		CleanChildren();
		this.gameObject.SetActive(false);
		this.gameObject.transform.position = new Vector3(0,-100,0);
	}

	public void Summon(Vector3 pos){
		this.gameObject.SetActive(true);
		this.transform.position = pos;
	}

	void CleanChildren(){
		SelectWeapon[] options = this.GetComponentsInChildren<SelectWeapon>();
		foreach(SelectWeapon opt in  options)
			opt.CleanSelect();
	}
}
