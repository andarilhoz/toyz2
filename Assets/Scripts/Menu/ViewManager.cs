﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ViewManager : MonoBehaviour {

	private MenuManager menuManager;
	// Use this for initialization
	void Start () {		
		menuManager = GameObject.Find("Menu").GetComponent<MenuManager>();
	}

	public void setCreditsView(){
		menuManager.changeState(MenuManager.State.Credits);
	}
	public void setFasesView(){
		menuManager.changeState(MenuManager.State.Fase);
	}

	public void setStatusView(){
		menuManager.changeState(MenuManager.State.Status);
	}

	public void setMenuView(){
		menuManager.changeState(MenuManager.State.Menu);
	}

	public void setConfigurationsView(){
		menuManager.changeState(MenuManager.State.Configurations);
	}

	public void setInformationsView(){
		menuManager.changeState(MenuManager.State.InformationsMenu);
	}
	
	public void setClasseView(string character){
		menuManager.changeState(MenuManager.State.CharacterMenu, character);
	}
	public void setDetailsView(){
		menuManager.changeState(MenuManager.State.Details);
	}

	public void exit(){
		Application.Quit();
	}

	public void Play(){
		LoadingManager.scene = 2;
		SceneManager.LoadScene(0);
	}
	public void setFaseRoom(){
		GameManager.fase = Fase.Room;
		this.Play();
	}
	public void setFaseLivingRoom(){
		GameManager.fase = Fase.LivingRoom;
		this.Play();
	}
	public void setFaseKitchem(){
		GameManager.fase = Fase.Kitchen;
		this.Play();
	}
}
