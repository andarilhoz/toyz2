﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationsController : MonoBehaviour {

	public Slider musicSlider;
	public Slider sfxSlider;
	public PlayerPrefController configurations;

	void Awake(){
		musicSlider.value = configurations.getMusicVolume();
		sfxSlider.value = configurations.getSfxVolume();
	}

	void Start(){
		musicSlider.onValueChanged.AddListener(delegate {
			MusicSliderChange();
		});
		sfxSlider.onValueChanged.AddListener(delegate {
			SfxSliderChange();
		});
	}

	void SfxSliderChange(){
		configurations.setSfxVolume(sfxSlider.value);
	}

	void MusicSliderChange(){
		configurations.setMusicVolume(musicSlider.value);
	}
	
}
