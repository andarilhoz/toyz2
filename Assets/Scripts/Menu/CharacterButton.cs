using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterButton : MonoBehaviour{	

    public List<Sprite> EnemySprites = new List<Sprite>();
    public List<Sprite> TowerSprites = new List<Sprite>();
    private Canvas canvas;
    private float widthPosition;
    private float highPosition;

    private ViewManager view;
    private MenuManager menu;
    
    public CharacterButton instantiateButton(string name, float widthPosition,
     float highPosition, Canvas canvas, ViewManager view, MenuManager menu){
        this.canvas = canvas;
        this.view = view;
        this.menu = menu;
        this.name = name;
        this.widthPosition = widthPosition;
        this.highPosition = highPosition;
		this.setButtonPosition()
        .setAvatarImageBasedOnName()
        .namingButton()
        .onClickActions();
        return this;
	}
    private CharacterButton setAvatarImageBasedOnName(){
        Image avatar = this.GetComponentInChildren<Image>();
        if(menu.getActualCharacterList() == Character.Enemy){
            avatar.sprite = EnemySprites.Find(element => element.name == this.name);
        }else if(menu.getActualCharacterList() == Character.Tower){
            avatar.sprite = TowerSprites.Find(element => element.name == this.name);
        }
        return this;
    }
	private CharacterButton setButtonPosition(){
		this.transform.SetParent(canvas.transform,false);
		this.transform.localPosition = new Vector3(this.widthPosition,this.highPosition,0);
		this.transform.localScale = new Vector3(1,1,1);
		return this;
	}

	private CharacterButton namingButton(){
		var buttonText = this.GetComponentInChildren<Text>();
		buttonText.text = name;
		return this;
	}

    public void destroy(){
        UnityEngine.Object.Destroy(this.gameObject);
    }

    private CharacterButton onClickActions () {
        Button b = this.gameObject.GetComponent<Button>();
        b.onClick.AddListener(delegate(){
            menu.setActualCharacter(this.name);
            view.setDetailsView();
        });
        return this;
    }

}