﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {


	public GameObject menuView;

	public GameObject creditsView;
	public GameObject faseView;
	public GameObject configurationsView;
	public GameObject statusView;
	public GameObject informationsView;
	public GameObject classeView;
	public GameObject detailsView;

	public AudioController audioController;
	
	public CharacterInfoList<Tower> towers;
	public CharacterInfoList<ArchEnemy> archEnemys;

	public TextAsset towerJSON;
	public TextAsset enemyJSON;

	public enum State  {
		Menu,
		Fase,
		Configurations,
		Status,
		InformationsMenu,
		CharacterMenu,
		Details,
		Credits,
	};
	private State state;
	private Character actualCharacterList;
	private string actualCharacter;


	void Start () {
		towers = CharacterInfoList<Tower>.LoadJson(towerJSON);
		archEnemys = CharacterInfoList<ArchEnemy>.LoadJson(enemyJSON);
		NextState();
	}

	IEnumerator MenuState() {
		Debug.Log("Menu: Enter");
		menuView.SetActive(true);
		while(state == State.Menu) {
			yield return 0;
		}
		menuView.SetActive(false);
		Debug.Log("Menu: Exited");
		NextState();
	}

	IEnumerator CreditsState() {
		Debug.Log("Credits: Enter");
		creditsView.SetActive(true);
		while(state == State.Credits) {
			yield return 0;
		}
		creditsView.SetActive(false);
		Debug.Log("Credits: Exited");
		NextState();
	}

	IEnumerator FaseState() {
		Debug.Log("Fase: Enter");
		faseView.SetActive(true);
		while(state == State.Fase) {
			yield return 0;
		}
		faseView.SetActive(false);
		Debug.Log("Fase: Exited");
		NextState();
	}

	IEnumerator StatusState() {
		Debug.Log("Status: Enter");
		statusView.SetActive(true);
		while(state == State.Status) {
			yield return 0;
		}
		statusView.SetActive(false);
		Debug.Log("Status: Exited");
		NextState();
	}

	IEnumerator ConfigurationsState() {
		Debug.Log("Configurations: Enter");
		configurationsView.SetActive(true);
		while(state == State.Configurations) {
			yield return 0;
		}
		configurationsView.SetActive(false);
		Debug.Log("Configurations: Exited");
		NextState();
	}

	IEnumerator InformationsMenuState() {
		Debug.Log("InformationsMenu: Enter");		
		informationsView.SetActive(true);
		while(state == State.InformationsMenu) {
			yield return 0;
		}
		informationsView.SetActive(false);
		Debug.Log("InformationsMenu: Exited");
		NextState();
	}

	IEnumerator CharacterMenuState() {
		Debug.Log("CharacterMenu: Enter");

		classeView.SetActive(true);

		
		while(state == State.CharacterMenu) {
			yield return 0;
		}
		classeView.SetActive(false);
		Debug.Log("CharacterMenu: Exited");
		NextState();
	}
	
	IEnumerator DetailsState() {
		Debug.Log("Details: Enter");
		detailsView.SetActive(true);
		while(state == State.Details) {
			yield return 0;
		}
		detailsView.SetActive(false);
		Debug.Log("Details: Exited");
		NextState();
	}

	void NextState() {
		string methodName = state.ToString() + "State";
		System.Reflection.MethodInfo info = 
			GetType().GetMethod(methodName,
								System.Reflection.BindingFlags.NonPublic |
								System.Reflection.BindingFlags.Instance);
			StartCoroutine((IEnumerator)info.Invoke(this,null));
	}

	public void changeState(State state, string stringCharacter = "undefined"){
		audioController.playButtonClicked();
		if(this.state == State.InformationsMenu){
			this.actualCharacterList = (Character)System.Enum.Parse( typeof( Character ), stringCharacter );
		}
		this.state = state;
	}

	public Character getActualCharacterList(){
		return this.actualCharacterList;
	}
	public string getActualCharacter(){
		return this.actualCharacter;
	}
	public State getState(){
		return this.state;
	}
	public void setActualCharacter(string character){
		this.actualCharacter = character;
	}




}
