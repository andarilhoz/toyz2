﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailsManager : MonoBehaviour {
	

	public List<Sprite> CharacterSprites = new List<Sprite>();
	public List<Sprite> TagSprites = new List<Sprite>();

	public Image avatar;
	public Image tagImage;

	private MenuManager menu;
	private ViewManager view;

	public Button exitButton;


	public ShadowText characterName;
	public ShadowText life;
	public ShadowText speed;
	public ShadowText damage;
	public ShadowText description;
	public ShadowText type;
	
	// Use this for initialization
	void Awake () {
		menu = GameObject.Find("Menu").GetComponent<MenuManager>();
		view = GameObject.Find("Main Camera").GetComponent<ViewManager>();
		this.onClickExit();
		this.changeTexts();
	}

	void OnEnable(){
		this.changeTexts();
	}

	private void changeTexts(){
		Character actualList = this.menu.getActualCharacterList();
		string name = this.menu.getActualCharacter();
		
		if(actualList == Character.Tower){
			Tower tower = menu.towers.list.Find(element => element.name == name);
			towerDetails(tower);
			characterDetails(tower);
			setAvatarImageBasedOnName(tower);
			setTagImage(tower);
			setTagType(tower);
			setCharacterName(tower);
		}else if(actualList == Character.Enemy){
			ArchEnemy enemy = menu.archEnemys.list.Find(element => element.name == name);
			enemyDetails(enemy);
			characterDetails(enemy);
			setAvatarImageBasedOnName(enemy);
			setTagImage(enemy);
			setTagType(enemy);
			setCharacterName(enemy);
		}
		
		
	}

	private void setTagType(CharacterInfo character){
		type.changeText(character.tag.ToString().ToUpper());
	}
	private void setCharacterName(CharacterInfo character){
		characterName.changeText(character.name.ToUpper());
	}

	private void setTagImage(CharacterInfo character){
		tagImage.sprite = TagSprites.Find(element => element.name == character.tag.ToString()); 
	}

	private void setAvatarImageBasedOnName(CharacterInfo character){
            avatar.sprite = CharacterSprites.Find(element => element.name == character.name);
    }

	private void characterDetails(CharacterInfo character){
		life.changeText(character.health.ToString());
		damage.changeText(character.damage.ToString());
		description.changeText(character.textBR.ToUpper());
	} 
	private void enemyDetails(ArchEnemy enemy){
		speed.changeText(enemy.speed.ToString());
	}
	private void towerDetails(Tower tower){
		speed.changeText(tower.rateOfFire.ToString());
	}

	private void onClickExit(){
		Character actualList = menu.getActualCharacterList();
		exitButton.onClick.AddListener(delegate(){
			view.setClasseView(actualList.ToString());
		});
	}
}
