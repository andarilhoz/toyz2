﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonagemBuilder : MonoBehaviour {

	public GameObject characterItem;
	public GameObject canvasObject;
	public GameObject labelClasse;
	public int numberOfRows;
	public float offSetValue;
	private MenuManager menu;
	private ViewManager view;
	private Canvas canvas;

	private List<CharacterButton> allActiveButtons = new List<CharacterButton>();

	// Use this for initialization
	void Awake (){
		canvas = canvasObject.GetComponent<Canvas>();
		menu = GameObject.Find("Menu").GetComponent<MenuManager>();
		view = GameObject.Find("Main Camera").GetComponent<ViewManager>();
	}
	void OnEnable () {
		this.InstantiateAllCharacterButtons(menu.getActualCharacterList());	
		labelClasse.GetComponentInChildren<Text>().text = menu.getActualCharacterList().ToString();	
	}
	
	void OnDisable () {
		foreach(CharacterButton g in allActiveButtons){
			g.destroy();
		}
		allActiveButtons = new List<CharacterButton>();
	}

	void InstantiateAllCharacterButtons(Character character){
		var total = 0;
		if(character == Character.Tower){
			total = menu.towers.list.Count;
			for( var i = 0; i < menu.towers.list.Count; i++){
				string name = menu.towers.list[i].name;
				initializeButtonScript(name,total,i);
			}
		}else if(character == Character.Enemy){
			total = menu.archEnemys.list.Count;
			for( var i = 0; i < menu.archEnemys.list.Count; i++){
				string name = menu.archEnemys.list[i].name;
				initializeButtonScript(name,total,i);
			}
		}
	}
	private void initializeButtonScript(string name, int total, int index){
		GameObject buttonObject = Object.Instantiate(characterItem);
		CharacterButton b = buttonObject.GetComponent<CharacterButton>();
		float widthPos = widthPositionToPlace(total,index);
		float hightPos = hightPositionToPlace(index);
		CharacterButton finalButton = b.instantiateButton(name, widthPos, hightPos,canvas,view,menu);
		allActiveButtons.Add(finalButton);
	}

	private float widthPositionToPlace(int total,int index){
		float distance = getDistance(total);
		float posBasedOnRow = index%numberOfRows -1;
		return distance * posBasedOnRow;
	}
	private float getDistance(int total){
		float totalOffset = offSetValue * (numberOfRows+1);
		return totalOffset / numberOfRows;
	}
	private float hightPositionToPlace(int actual){
		float numberOfLine = Mathf.Ceil(actual/numberOfRows);
		float adjust = -90;
		return numberOfLine * adjust;
	}
}
