﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMoveHUDController : MonoBehaviour {

	// Use this for initialization
	private static PlayMoveHUDController instance;

    public static PlayMoveHUDController Instance { get { return instance; } }

	void Awake(){
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
		DontDestroyOnLoad(gameObject);
	}
	public void ToggleTurnScreen(){
		if(Screen.orientation == ScreenOrientation.LandscapeLeft)
			Screen.orientation = ScreenOrientation.LandscapeRight;
		else if(Screen.orientation == ScreenOrientation.LandscapeRight)
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		else
			Screen.orientation = ScreenOrientation.LandscapeLeft;
	}

}
