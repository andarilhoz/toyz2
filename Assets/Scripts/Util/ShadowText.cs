using UnityEngine;
using UnityEngine.UI;

public class ShadowText : MonoBehaviour{	

    public Text originalText;
    public Text shadowedText;
    
    public ShadowText instantiateText(string text){
        this.originalText.text = text;
        this.shadowedText.text = text;
        return this;
	}

    public ShadowText changeText(string text){
        this.originalText.text = text;
        this.shadowedText.text = text;
        return this;
    }
    

}