﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

	// Use this for initialization
	public AudioClip menuTheme;
	public AudioClip buttonClick;
	public AudioSource themeAudioSource;
	public AudioSource buttonAudioSource;

	public PlayerPrefController configurations;

	void OnEnable(){
		PlayerPrefController.onMusicVolumeChange += ChangeVolume;
	}
	
	void OnDisable(){
		PlayerPrefController.onMusicVolumeChange -= ChangeVolume;
	}
	void Start () {
		TocaRaul();
	}

	private void TocaRaul(){
		AudioSource audioSource = this.GetComponentInChildren<AudioSource>();
		this.ChangeVolume();
		audioSource.clip = menuTheme;
		audioSource.Play();
	}

	private void ChangeVolume(){
		themeAudioSource.volume = configurations.getMusicVolume();
	}

	public void playButtonClicked(){
		buttonAudioSource.volume = configurations.getSfxVolume();
		buttonAudioSource.clip = buttonClick;
		buttonAudioSource.Play();
	}
}
