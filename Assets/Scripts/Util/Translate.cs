﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Translate : MonoBehaviour {
	private MenuManager menu;
	// Use this for initialization
	private Text[] viewTexts;
	void Start () {
		menu = GameObject.Find("Menu").GetComponent<MenuManager>();
		viewTexts = GetComponentsInChildren<Text>();
		translateAllComponents();
	}
	void OnEnable () {
		if(validateTextArray(this.viewTexts)){
			this.translateAllComponents();
		}
	}
	public void translateAllComponents(){
		foreach (Text guiText in this.viewTexts){
			if(guiText.IsActive() && guiText.tag == "characterLabel")
				this.translateCharacter(guiText);
		}
	}

	public bool validateTextArray(Text[] text){
		return this.viewTexts != null && this.viewTexts.Length > 0;
	}
	private void translateCharacter(Text guiText){
		guiText.text = menu.getActualCharacterList().ToString();
	}
}
