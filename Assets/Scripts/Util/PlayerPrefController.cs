﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefController : MonoBehaviour {

	private float musicVolumeValue;
	private float sfxVolumeValue;
	
	public delegate void musicVolume();
	public static event musicVolume onMusicVolumeChange;

	void Awake(){
		musicVolumeValue = PlayerPrefs.GetFloat("musicVolume",1);
		sfxVolumeValue = PlayerPrefs.GetFloat("sfxVolume",1);
	}

	// Use this for initialization
	void Start () {
		
	}

	public void setMusicVolume(float volume){
		this.musicVolumeValue = volume;
		PlayerPrefs.SetFloat("musicVolume",volume);
		onMusicVolumeChange();
	}

	public float getMusicVolume(){
		musicVolumeValue = PlayerPrefs.GetFloat("musicVolume",1);
		return this.musicVolumeValue;
	}

	public void setSfxVolume(float volume){
		this.sfxVolumeValue = volume;
		PlayerPrefs.SetFloat("sfxVolume",volume);
	}

	public float getSfxVolume(){
		musicVolumeValue = PlayerPrefs.GetFloat("sfxVolume",1);
		return this.sfxVolumeValue;
	}

}
