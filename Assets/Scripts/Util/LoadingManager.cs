﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour {

	[SerializeField]
	private Text textTip; 

	[SerializeField]
	private Text loadingText;
	
	[SerializeField]
	public static int scene = 1;

	// Use this for initialization

	
 	private AsyncOperation async = null;
	void Start () {
		
		StartCoroutine(this.loadScene());
	}
	
	// Update is called once per frame
	void Update () {
		if(async != null){
			loadingText.text = Mathf.Ceil((async.progress * 100)).ToString() + "%" ;
		}
	}

	private IEnumerator loadScene(){
		//yield return new WaitForSeconds(1f);
		async = SceneManager.LoadSceneAsync(scene);
		yield return async;
	}

}
