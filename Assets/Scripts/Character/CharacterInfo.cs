using UnityEngine;
public class CharacterInfo {
    public string name;
    public int damage;
    public int health;
    public Tag tag;
    public Target targetType;
    public int value;
    public GameObject prefab;
    public string textBR;
    public string textUS;
}
