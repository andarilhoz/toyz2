using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


[Serializable]
public class CharacterInfoList<T> {

    public string name;
    public List<T> list;
    public static CharacterInfoList<T> LoadJson(TextAsset file) {
        CharacterInfoList<T> listOfCharacters  = JsonUtility.FromJson<CharacterInfoList<T>>(file.text);
        return listOfCharacters;
    }
}
