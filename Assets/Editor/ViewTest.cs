﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class ViewTest {

	[Test]
	public void CameraShouldHaveViewManagerScriptAttached() {
		GameObject camera = GameObject.Find("Main Camera");
		Assert.That(camera.GetComponent<ViewManager>());
	}

}
