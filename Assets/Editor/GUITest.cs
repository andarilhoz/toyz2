﻿using NUnit.Framework;
using UnityEngine;

public class GUITest {
		
	[Test]
	public void ThereIsACanvas(){
		Assert.That(GameObject.Find("Canvas"));
	}

	[Test]
	public void ThereIsAnEventSystem(){
		Assert.That(GameObject.Find("EventSystem"));
	}

	[Test]
	public void ThereIsAGUIManagerScriptAttachedToCanvas(){
		GameObject canvas = GameObject.Find("Canvas");
		Assert.That(canvas.GetComponent<GUIManager>());
	}

	[TestFixture]
    [Category("StateMachine Tests")]
    internal class StateMachineTests{

		[Test]
		public void ChangeStateIsPossible() {
			GameManager manager = GameObject.Find("GameManager").GetComponent<GameManager>();
			manager.changeState(GameManager.State.Pause);
			manager.changeState(GameManager.State.Game);
			Assert.That(manager.getState() == GameManager.State.Game);
		}
	}

}
