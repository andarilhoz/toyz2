﻿using NUnit.Framework;
using UnityEngine;

public class MenuTest {
		
	[Test]
	public void ThereIsACanvas(){
		Assert.That(GameObject.Find("Canvas"));
	}

	[Test]
	public void ThereIsAnEventSystem(){
		Assert.That(GameObject.Find("EventSystem"));
	}

	[Test]
	public void ThereIsAGUIManagerScriptAttachedToCanvas(){
		GameObject canvas = GameObject.Find("Menu");
		Assert.That(canvas.GetComponent<GUIManager>());
	}

	[TestFixture]
    [Category("StateMachine Tests")]
    internal class StateMachineTests{

		[Test]
		public void ChangeStateIsPossible() {
			GameObject gui = GameObject.Find("Menu");
			MenuManager manager = gui.GetComponent<MenuManager>();
			manager.changeState(MenuManager.State.Menu);
			manager.changeState(MenuManager.State.CharacterMenu);
			Assert.That(manager.getState() == MenuManager.State.CharacterMenu);
		}
	}

}
